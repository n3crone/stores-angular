# https://docs.docker.com/develop/develop-images/multistage-build/#stop-at-a-specific-build-stage
# https://docs.docker.com/compose/compose-file/#target


# https://docs.docker.com/engine/reference/builder/#understand-how-arg-and-from-interact
ARG NODE_VERSION=13
ARG NGINX_VERSION=1.17


# "development" stage
FROM node:${NODE_VERSION}-alpine AS api_platform_client_development

WORKDIR /usr/src/client

RUN yarn global add @api-platform/client-generator

COPY package.json ./

#Fix that greedy install
RUN set -eux; \
	npm install \
	ng update \
	npm update

RUN npm install -g @angular/cli@8.0.6
RUN npm install --save-dev @angular-devkit/build-angular
RUN npm install --save-dev node-sass
RUN npm install
RUN ng update
RUN npm update
RUN npm install

COPY . ./

VOLUME /usr/src/client/node_modules

ENV HTTPS true

CMD ng serve --host 0.0.0.0 --poll 1

# "build" stage
# depends on the "development" stage above
FROM api_platform_client_development AS api_platform_client_build

ARG REACT_APP_API_ENTRYPOINT

#Fix that greedy install
RUN set -eux; \
	npm install \
	ng update \
	npm update

RUN npm install -g @angular/cli@8.0.6
RUN npm install --save-dev @angular-devkit/build-angular
RUN npm install --save-dev node-sass
RUN npm install
RUN ng update
RUN npm update
RUN npm install


# "nginx" stage
# depends on the "build" stage above
FROM nginx:${NGINX_VERSION}-alpine AS api_platform_client_nginx

COPY docker/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf

WORKDIR /usr/src/client/build

COPY --from=api_platform_client_build /usr/src/client/build ./
