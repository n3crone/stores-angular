import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminPageComponent} from './modules/admin/pages/admin/admin-page.component';
import {HomePageComponent} from './modules/home/pages/home/home-page.component';
import {LoginPageComponent} from './modules/login/pages/login/login-page.component';
import {ProductListPageComponent} from './modules/product/pages/list/list.page';
import {ScrapperListPageComponent} from './modules/scrapper/pages/list/list.page';
import {ShopListPageComponent} from './modules/shop/pages/list/list.page';

const routes: Routes = [
  {path: 'products', component: ProductListPageComponent},
  {path: 'scrappers', component: ScrapperListPageComponent},
  {path: 'shops', component: ShopListPageComponent},
  {path: 'login', component: LoginPageComponent},
  {path: 'admin', component: AdminPageComponent},
  {path: 'home', component: HomePageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
