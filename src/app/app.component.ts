import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {Role} from './shared/model/role';
import {User} from './shared/model/user';
import {AuthenticationService} from './shared/services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'outlet-angular';
  currentUser: User;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
  ) {
    this.authenticationService.currentUser.subscribe((user) => this.currentUser = user);
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.roles.includes(Role.ROLE_ADMIN);
  }

  get isUser() {
    return this.currentUser && this.currentUser.roles.includes(Role.ROLE_USER);
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
