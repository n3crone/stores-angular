import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import 'hammerjs';

import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MaterialModule} from './material.module';
import {AdminModule} from './modules/admin/admin.module';
import {HomeModule} from './modules/home/home.module';
import {LoginModule} from './modules/login/login.module';
import {ProductDetailComponent} from './modules/product/components/detail/detail.component';
import {ProductListPageComponent} from './modules/product/pages/list/list.page';
import {ProductModule} from './modules/product/product.module';
import {ScrapperModule} from './modules/scrapper/scrapper.module';
import {ShopModule} from './modules/shop/shop.module';
import {ErrorInterceptor} from './shared/helpers/error.interceptor';
import {JwtInterceptor} from './shared/helpers/jwt.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    ProductListPageComponent,
    ProductDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    ScrapperModule,
    ShopModule,
    ProductModule,
    AdminModule,
    HomeModule,
    LoginModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
