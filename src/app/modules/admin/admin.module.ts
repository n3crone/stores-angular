import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {AdminComponent} from './components/admin/admin.component';
import {AdminPageComponent} from './pages/admin/admin-page.component';

@NgModule({
  declarations: [AdminComponent, AdminPageComponent],
  imports: [
    CommonModule,
  ],
})
export class AdminModule {
}
