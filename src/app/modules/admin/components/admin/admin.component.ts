import {Component, OnInit} from '@angular/core';
import {first} from 'rxjs/operators';

import {User} from '../../../../shared/model/user';
import {UserService} from '../../../../shared/services/user.service';

@Component({
  selector: 'app-admin',
  templateUrl: 'admin.component.html',
})
export class AdminComponent implements OnInit {
  users: User[] = [];

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.userService.getAll().pipe(first()).subscribe((users) => {
      this.users = users['hydra:member'];
    });
  }
}
