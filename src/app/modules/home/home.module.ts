import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {HomeComponent} from './components/home/home.component';
import {HomePageComponent} from './pages/home/home-page.component';

@NgModule({
  declarations: [HomeComponent, HomePageComponent],
  imports: [
    CommonModule,
  ],
})
export class HomeModule {
}
