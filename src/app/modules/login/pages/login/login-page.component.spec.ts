import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ShopListPage} from './login-page.component';

describe('ShopListPage', () => {
  let component: ShopListPage;
  let fixture: ComponentFixture<ShopListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShopListPage],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
