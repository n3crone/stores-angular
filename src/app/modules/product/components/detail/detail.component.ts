import {Component, OnInit} from '@angular/core';
import {Product} from '../../../../shared/model/product';
import {ApiService} from '../../../../shared/services/api.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class ProductDetailComponent implements OnInit {
  product: Product;

  constructor(private apiService: ApiService) {
  }

  getProduct(): void {
    this.apiService.getProduct(1)
      .subscribe((product) => this.product = product);
  }

  ngOnInit() {
    this.getProduct();
  }

}
