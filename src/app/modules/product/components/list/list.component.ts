import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {MatPaginator, MatSort} from '@angular/material';
import {merge} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Product} from '../../../../shared/model/product';
import {ApiService} from '../../../../shared/services/api.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ProductListComponent implements OnInit, AfterViewInit {
  products: Product[];
  productsSearch: NgForm;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  productsLength: number;
  rowItems: number;
  isGridLayout = 0;
  loading: boolean;
  displayedColumns: string[] = [
    'id', 'image', 'baseInfo', 'additionalInfo', 'normalPrice', 'outletPrice', 'discount', 'quality',
    'updatedAt', 'createdAt', 'shopName', 'link',
  ];

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
    this.onResize();
    this.getProducts(this.productsSearch);
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.getProducts(this.productsSearch)),
      )
      .subscribe();
  }

  getProducts(productSearch: NgForm): void {
    this.smoothScroll();
    this.productsSearch = productSearch;
    this.apiService.getProducts(this.paginator, this.sort, this.productsSearch)
      .subscribe((products) => {
        this.paginator.pageSize = products['hydra:member'].length;
        this.productsLength = products['hydra:totalItems'];
        this.products = products['hydra:member'];
        this.loading = false;
      });
  }

  smoothScroll() {
    this.loading = true;
    const scrollToTop = (window).setInterval(() => {
      const pos = window.pageYOffset;
      if (pos <= 0) {
        window.clearInterval(scrollToTop);
        return;
      }

      window.scrollTo(0, pos - 5);
      document.querySelector('.mat-sidenav-content').scrollTo(0, pos * 15 - 50);
    }, 16);
  }

  onResize() {
    if (window.innerWidth > 1860) {
      this.rowItems = 6;
    } else if (window.innerWidth > 1550) {
      this.rowItems = 5;
    } else if (window.innerWidth > 1240) {
      this.rowItems = 4;
    } else if (window.innerWidth > 930) {
      this.rowItems = 3;
    } else if (window.innerWidth > 620) {
      this.rowItems = 2;
    } else {
      this.rowItems = 1;
    }
  }

  changeLayout(isGrid): void {
    this.isGridLayout = isGrid.index;
  }
}
