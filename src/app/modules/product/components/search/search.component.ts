import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ProductSearch} from '../../../../shared/model/productSearch';
import {Shop} from '../../../../shared/model/shop';
import {ApiService} from '../../../../shared/services/api.service';

@Component({
  selector: 'app-product-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class ProductSearchComponent implements OnInit {
  @ViewChild('searchForm') searchForm: NgForm;
  @Output() searchFormSubmit = new EventEmitter<NgForm>();
  shops: Shop[];
  productSearch: ProductSearch;

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
    this.getShops();
    this.productSearch = new ProductSearch();
  }

  getShops(): void {
    this.apiService.getShops()
      .subscribe((shops) => {
        this.shops = shops['hydra:member'];
      });
  }

  onSubmit(productSearch: NgForm) {
    this.searchFormSubmit.emit(productSearch);
  }
}
