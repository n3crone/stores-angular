import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MaterialModule} from '../../material.module';
import {ProductListComponent} from './components/list/list.component';
import {ProductSearchComponent} from './components/search/search.component';

@NgModule({
  declarations: [ProductListComponent, ProductSearchComponent],
  exports: [
    ProductListComponent,
    ProductSearchComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ],
})
export class ProductModule {
}
