import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ScrapperListComponent} from './scrapper-home.component';

describe('ScrapperListComponent', () => {
  let component: ScrapperListComponent;
  let fixture: ComponentFixture<ScrapperListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ScrapperListComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrapperListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
