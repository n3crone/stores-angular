import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material';
import {tap} from 'rxjs/operators';
import {Scrapper} from '../../../../shared/model/scrapper';
import {ApiService} from '../../../../shared/services/api.service';

@Component({
  selector: 'app-scrapper-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ScrapperListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = [
    'id', 'shopIcon', 'shopName', 'page', 'newItems', 'updatedItems', 'datetimeStart', 'datetimeEnd', 'datetimeUpdate',
  ];
  loading: boolean;
  scrappers: Scrapper[];
  scrappersLength: number;
  currentDate = new Date();

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
    this.loading = true;
    this.paginator.page.pipe(
      tap(() => this.getScrappers()),
    )
      .subscribe();

    this.getScrappers();
  }

  getScrappers() {
    this.apiService.getScrappers(this.paginator)
      .subscribe((scrappers) => {
        this.paginator.pageSize = scrappers['hydra:member'].length;
        this.scrappersLength = scrappers['hydra:totalItems'];
        this.scrappers = scrappers['hydra:member'];
        this.loading = false;
      });
  }

  compareScrapperDates(scrapperDate) {
    const diff = Math.abs(this.currentDate.getTime() - new Date(scrapperDate).getTime());
    const minutes = Math.floor((diff / 1000) / 60);
    if (minutes > 5) {
      return true;
    }
  }

}
