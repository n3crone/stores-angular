import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ScrapperListPageComponent} from './list.page';

describe('ScrapperListPage', () => {
  let component: ScrapperListPageComponent;
  let fixture: ComponentFixture<ScrapperListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ScrapperListPageComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrapperListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
