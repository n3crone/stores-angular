import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MaterialModule} from '../../material.module';
import {ScrapperListComponent} from './components/list/list.component';
import {ScrapperListPageComponent} from './pages/list/list.page';

@NgModule({
  declarations: [ScrapperListComponent, ScrapperListPageComponent],
  imports: [
    CommonModule,
    MaterialModule,
  ],
})
export class ScrapperModule {
}
