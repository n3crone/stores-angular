import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material';
import {tap} from 'rxjs/operators';
import {Shop} from '../../../../shared/model/shop';
import {ApiService} from '../../../../shared/services/api.service';

@Component({
  selector: 'app-shop-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ShopListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'imagePath', 'name', 'productsCount', 'lastScrappedTime'];
  loading: boolean;
  shops: Shop[];
  shopsLength: number;
  currentDate = new Date();

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
    this.loading = true;
    this.paginator.page.pipe(
      tap(() => this.getShops()),
    )
      .subscribe();

    this.getShops();
  }

  getShops() {
    this.apiService.getShops(this.paginator)
      .subscribe((scrappers) => {
        this.paginator.pageSize = scrappers['hydra:member'].length;
        this.shopsLength = scrappers['hydra:totalItems'];
        this.shops = scrappers['hydra:member'];
        this.loading = false;
      });
  }

  compareShopDates(scrapperDate) {
    const diff = Math.abs(this.currentDate.getTime() - new Date(scrapperDate).getTime());
    const minutes = Math.floor((diff / 1000) / 60);
    if (minutes > 1440) {
      return true;
    }
  }

}
