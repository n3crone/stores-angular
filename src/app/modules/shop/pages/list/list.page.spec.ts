import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ShopListPageComponent} from './list.page';

describe('ShopListPage', () => {
  let component: ShopListPageComponent;
  let fixture: ComponentFixture<ShopListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShopListPageComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
