import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MaterialModule} from '../../material.module';
import {ShopListComponent} from './components/list/list.component';
import {ShopListPageComponent} from './pages/list/list.page';

@NgModule({
  declarations: [ShopListPageComponent, ShopListComponent],
  imports: [
    CommonModule,
    MaterialModule,
  ],
})
export class ShopModule {
}
