export class Product {
  id: number;
  baseInfo: string;
  additionalInfo: string;
  normalPrice: number;
  outletPrice: number;
  quality: string;
  discount: number;
  updatedAt: string;
  createdAt: string;
  shop: string;
  shopName: string;
  link: string;
  imagePath: string;
  shopIcon: string;
}
