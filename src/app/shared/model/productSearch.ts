export class ProductSearch {
  outletPriceGte: number;
  outletPriceLte: number;
  normalPriceGte: number;
  normalPriceLte: number;
  link: string;
  baseInfo: string;
  shops: string[];
}
