export class Scrapper {
  id: number;
  datetimeStart: string;
  datetimeEnd: string;
  datetimeUpdate: string;
  page: number;
  newItems: number;
  updatedItems: number;
  shopName: string;
  shopIcon: string;
}
