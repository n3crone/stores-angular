export class Shop {
  id: number;
  name: string;
  imagePath: string;
  productsCount: number;
  lastScrappedTime: string;
}
