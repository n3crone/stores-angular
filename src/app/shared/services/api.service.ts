import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {NgForm} from '@angular/forms';
import {MatPaginator, MatSort} from '@angular/material';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Product} from '../model/product';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  public filesEndpoint = environment.apiUrl + environment.endpoints.files;
  private productsEndpoint = environment.apiUrl + environment.endpoints.products;
  private shopsEndPoint = environment.apiUrl + environment.endpoints.shops;
  private scrappersEndpoint = environment.apiUrl + environment.endpoints.scrappers;
  private params: HttpParams;

  constructor(private http: HttpClient) {
  }

  getProducts(paginator: MatPaginator, sort: MatSort, form: NgForm): Observable<Product[]> {
    this.buildHttpParams(paginator, sort, form);
    return this.http.get<Product[]>(this.productsEndpoint, {params: this.params});
  }

  getShops(paginator?: MatPaginator): Observable<Product[]> {
    this.buildHttpParams(paginator);
    return this.http.get<Product[]>(this.shopsEndPoint);
  }

  getScrappers(paginator: MatPaginator): Observable<Product[]> {
    this.buildHttpParams(paginator);
    return this.http.get<Product[]>(this.scrappersEndpoint, {params: this.params});
  }

  getProduct(id: number): Observable<Product> {
    return this.http.get<Product>(`${this.productsEndpoint}/${id}`);
  }

  private buildHttpParams(paginator?: MatPaginator, sort?: MatSort, form?: NgForm) {
    this.params = new HttpParams();
    if (paginator) {
      this.addPaginationParams(paginator);
    }

    if (sort) {
      this.addSortParams(sort);
    }

    if (form) {
      this.addFormParams(form);
    }
  }

  private addPaginationParams(paginator: MatPaginator) {
    this.params = this.params.append('page', (paginator.pageIndex + 1).toString());
    this.params = paginator.pageSize
      ? this.params.append('itemsPerPage', paginator.pageSize.toString())
      : this.params;
  }

  private addSortParams(sort: MatSort) {
    this.params = sort.active
      ? this.params.append('order[' + sort.active + ']', sort.direction)
      : this.params;
  }

  private addFormParams(form: NgForm) {
    const httpParams = form.value;
    Object.keys(httpParams).forEach((key) => {
      if (!httpParams[key]) {
        return;
      }

      if (key !== 'shop') {
        this.params = this.params.append(key, httpParams[key]);
        return;
      }

      Object.values(httpParams[key]).forEach((nested) => {
        this.params = this.params.append(key + '[]', '/shops/' + nested as string);
      });
    });
  }
}
